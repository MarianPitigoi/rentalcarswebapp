import './App.css';
import { Route, Switch } from 'react-router';
import Home from './components/Home';
import CarInfo from './components/CarInfo';
import AddCar from './components/AddCar'
import AddCarInfo from './components/AddCarInfo';
import AvailableCars from './components/AvailableCars';
import AddReservation from './components/AddReservation';
import ViewReservations from './components/ViewReservations';
import ViewUserReservation from './components/ViewUserReservation';
import AddCarReservation from './components/AddCarReservation';
import EditCarInfo from './components/EditCarInfo';
// import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Switch >
        <Route exact path="/" render={(props) => <Home {...props} /> } />
        <Route exact path="/availablecars/:pickupDate/:returnDate" render={(props) => <AvailableCars {...props} /> } />
        <Route exact path="/rentalcars/:carId" render={(props) => <CarInfo {...props} /> } />
        <Route exact path="/rentalcars/:carId/addcarinfo" render={(props) => <AddCarInfo {...props} /> } />
        <Route exact path="/reservation/:carId/:pickupDate/:returnDate" render={(props) => <AddReservation {...props} /> } />
        <Route exact path="/reservation/:reservationId" render={(props) => <ViewUserReservation {...props} /> } />
        <Route exact path="/reservations" render={(props) => <ViewReservations {...props} /> } />
        <Route exact path="/addcar" render={(props) => <AddCar {...props} /> } />
        <Route exact path="/editcarinfo/:carId/:carInfoVin" render={(props) => <EditCarInfo {...props} />} />
        <Route exact path="/assigncartoreservation/:carId/:reservationId/:pickupDate/:returnDate" render={(props) => <AddCarReservation {...props} /> } />
      </Switch>
    </div>
  );
}

export default App;
