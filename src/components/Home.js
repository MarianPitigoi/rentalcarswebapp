import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";



class Home extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            pickupDay: new Date(),
            pickupHour: 12,
            returnDay: new Date(),
            returnHour: 12,
            cars: [],
            pickupHourOptions: [],
            returnHourOption: [],
        }

        this.handleDelete = this.handleDelete.bind(this);
        this.handleFindSubmit = this.handleFindSubmit.bind(this);
    }
    
    componentDidMount() {
        axios.get('http://localhost:8080/rentalcars').then(res => {
            const cars = res.data;
            this.setState({cars})
        });
    }

    handleDelete(car) {
        axios.delete(`http://localhost:8080/rentalcars/${car.id}`).then(() => {
            let updateCars = [...this.state.cars].filter(item => item.id !== car.id);
            this.setState({cars: updateCars});
        })
    }

    handleFindSubmit(event){
        event.preventDefault();
        let pickupDay = this.state.pickupDay;
        let pickupHour = this.state.pickupHour;
        let pickupDate = new Date(`${pickupDay.getFullYear()}-${pickupDay.getMonth() +1}-${pickupDay.getDate()} ${pickupHour}:00:00`);
        let returnDay = this.state.returnDay;
        let returnHour = this.state.returnHour;
        let returnDate = new Date(`${returnDay.getFullYear()}-${returnDay.getMonth() +1}-${returnDay.getDate()} ${returnHour}:00:00`)
        
        this.props.history.push(`/availablecars/${pickupDate}/${returnDate}`);
    }

    render() {

        let carsList = this.state.cars.map((car) => 
            <tr key={car.id}>
                <td>{car.brand}</td>
                <td>{car.model}</td>
                <td>{car.category}</td>
                <td>{car.transmission}</td>
                <td>{car.numberOfPassengers}</td>
                <td>{car.pricePerHour}</td>
                <td><Link to={'/rentalcars/' + car.id}>View Cars</Link></td>
                <td><Link to={'/'} onClick={() => this.handleDelete(car)}>Delete</Link></td>
            </tr>       
        )

        let addHours = () =>{
            let dayHours = [];
            for(let hour = 0; hour < 24; ++hour){
                dayHours[hour] = hour;
            }
            return dayHours;
        }

        let pickupHourOptions = addHours();

        let returnHourOption = addHours();

        let pickupHourOptionList = pickupHourOptions.map( (pickupHour) =>
            <option name={pickupHour} key={pickupHour}>{pickupHour}</option>
        )
        
        let returnHourOptionList = returnHourOption.map( (returnHour) =>
            <option name={returnHour} key={returnHour}>{returnHour}</option>
        )


        return (
            <div>
                <form>
                    Pickup Date: <DatePicker selected={this.state.pickupDay} onChange={(date) => {this.setState({pickupDay: date})}}/>
                    Pickup Hour: <select name="pickupHour" defaultValue="12" onChange={(event) => {
                        this.setState({pickupHour: event.target.value});
                        console.log("Pickup Hour: " + this.state.pickupHour);
                        }}>
                                    {pickupHourOptionList}
                                 </select> <br />
                    Return Date: <DatePicker selected={this.state.returnDay} onChange={(date) => {this.setState({returnDay: date})}}/>
                    Return Hour: <select name="returnHour" defaultValue="12" onChange={(event) => {this.setState({returnHour: event.target.value})}}>
                                    {returnHourOptionList}
                                 </select> <br />
                    <button onClick={this.handleFindSubmit}>Find</button>
                </form>

                <h2>All Models</h2>
                <table align='center' border='1px'>
                    <thead>
                        <tr>
                            <th>Brand</th>
                            <th>Model</th>
                            <th>Category</th>
                            <th>Transmission</th>
                            <th>Number of passegers</th>
                            <th>Price per hour</th>
                        </tr>
                    </thead>
                    <tbody>
                            {carsList}
                    </tbody>
                </table>

                <Link to='/addcar'>Add new Model</Link> <br />
                <Link to='/reservations'>View Reservations</Link>
            </div>
        )
    }
}

export default Home
