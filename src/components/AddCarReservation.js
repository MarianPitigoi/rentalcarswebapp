import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Navigation from './Navigation';

class AddCarReservation extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            reservation:{},
            carsInfo:[]
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    componentDidMount(){
        let carId = this.props.match.params.carId;
        let pickupDate = new Date(this.props.match.params.pickupDate);
        let returnDate = new Date(this.props.match.params.returnDate);
        let reservationId = this.props.match.params.reservationId;

        axios.get(`http://localhost:8080/carsInfo/${carId}/${pickupDate}/${returnDate}`).then(res => {
            let carsInfo = res.data;
            this.setState({carsInfo});
        });
        axios.get(`http://localhost:8080/rentalcars/reservations/${reservationId}`).then(res => {
            let reservation = res.data;
            this.setState({reservation});
            console.log(reservation);
        });

    }

    handleSubmit(carInfo){

        const data = {
            reservationId: this.props.match.params.reservationId,
            carInfoVin: carInfo.vin,
            carInfoAvailability: carInfo.available,
            carInfoNumberPlate: carInfo.numberPlate,
            carInfoOdometer: carInfo.odometer
        }
        axios.put(`http://localhost:8080/rentalcars/reservations`, data).then(() => {
            window.location ='/reservations';
        })
    }

    render() {
        // console.log('AddCarReservation');
        console.log(this.props);

        let pickupDate = new Date(this.state.reservation.reservationPickupDate).toLocaleString(['ro-RO']);
        let returnDate = new Date(this.state.reservation.reservationReturnDate).toLocaleString(['ro-RO']);

        let isAvailable = (available) => available ? 'available' : 'unavailable'; 

        let carInfoList = this.state.carsInfo.map((carInfo, index) =>
            <tr key={index}>
                <td>{carInfo.vin}</td>
                <td>{isAvailable(carInfo.available)}</td>
                <td>{carInfo.numberPlate}</td>
                <td>{carInfo.odometer}</td>
                <td><Link to={'/reservations'} onClick={() => this.handleSubmit(carInfo)}>Select</Link></td>
            </tr>
        )  

        return (
            <div>
                <div align='left'>
                    <Navigation />
                </div>
                <h2>Reservation:</h2><br />
                From: {pickupDate} <br />
                To: {returnDate} <br />
                Total Price: {this.state.reservation.reservationTotalPrice} <br />
                <br />
                <h3>User:</h3>
                First Name: {this.state.reservation.userFirstName} <br />
                Middle Name: {this.state.reservation.userMiddleName} <br />
                Last Name: {this.state.reservation.userLastName} <br />
                Email: {this.state.reservation.userEmail} <br />
                Phone Number: {this.state.reservation.userPhoneNumber} <br />
                <br />
                <h3>Car:</h3>
                Brand: {this.state.reservation.carBrand} <br />
                Model: {this.state.reservation.carModel} <br />
                Category: {this.state.reservation.carCategory} <br />
                Transmission: {this.state.reservation.carTransmission} <br />
                Number of Passengers: {this.state.reservation.carNumberOfPassengers} <br />
                Price per Hour: {this.state.reservation.carPricePerHour} <br />

                <table align='center' border='1px'>
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Available</th>
                            <th>Number Plate</th>
                            <th>Odometer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {carInfoList}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default AddCarReservation
