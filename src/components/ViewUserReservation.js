import React, { Component } from 'react';
import axios from 'axios';
import Navigation from './Navigation';

class ViewUserReservation extends Component {
    constructor(props) {
        super(props)
    
        this.state = {

        }
    }
    
    componentDidMount(){
        axios.get(`http://localhost:8080/rentalcars/reservations/${this.props.match.params.reservationId}`).then(res => {
            this.setState(res.data);
            console.log(this.state);
        });
    }

    render() {
        console.log('ViewUserReservation');
        console.log(this.props);

        let pickupDate = new Date(this.state.reservationPickupDate).toLocaleString(['ro-RO']);
        let returnDate = new Date(this.state.reservationReturnDate).toLocaleString(['ro-RO']);
        return (
            <div>
                <div align='left'>
                    <Navigation />
                </div>
                <h2>Reservation:</h2><br />
                From: {pickupDate} <br />
                To: {returnDate} <br />
                Total Price: {this.state.reservationTotalPrice} <br />
                <br />
                <h3>User:</h3>
                First Name: {this.state.userFirstName} <br />
                Middle Name: {this.state.userMiddleName} <br />
                Last Name: {this.state.userLastName} <br />
                Email: {this.state.userEmail} <br />
                Phone Number: {this.state.userPhoneNumber} <br />
                <br />
                <h3>Car:</h3>
                Brand: {this.state.carBrand} <br />
                Model: {this.state.carModel} <br />
                Category: {this.state.carCategory} <br />
                Transmission: {this.state.carTransmission} <br />
                Number of Passengers: {this.state.carNumberOfPassengers} <br />
                Price per Hour: {this.state.carPricePerHour} <br />

            </div>
        )
    }
}

export default ViewUserReservation
