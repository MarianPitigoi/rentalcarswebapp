import axios from 'axios';
import React, { Component } from 'react';
import Navigation from './Navigation';

class AddCar extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            id: 0,
            brand: '',
            model: '',
            category: 'small',
            transmission: 'manual',
            numberOfPassengers: 0,
            pricePerHour: 0,
            categoryOptions: [],
            transmissionOptions: []
        }
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    
    handleSubmit(event) {
        event.preventDefault();
        const data = {
            id:this.state.id,
            brand:this.state.brand,
            model:this.state.model,
            category:this.state.category,
            transmission:this.state.transmission,
            numberOfPassengers:this.state.numberOfPassengers,
            pricePerHour:this.state.pricePerHour
        }
        axios.post('http://localhost:8080/rentalcars', data).then(()=>{
            window.location ='/';
        })
    }

    render() {
        console.log('AddCar');
        console.log(this.props);
        
        let categoryOptions = ['small', 'medium', 'large', 'suv', 'luxury', 'van', 'commercial', 'convertible'];
        let transmissionOptions = ['manual', 'automatic'];
        

        let optionCategoryList = categoryOptions.map((categoty) => 
        <option value={categoty} key={categoty}>{categoty}</option>
        );

        let optionTransmissionList = transmissionOptions.map( (transmission) =>
        <option value={transmission} key={transmission}>{transmission}</option>
        );

        return (
            <div align='left'>
                <div align='left'>
                    <Navigation />
                </div>
                <h2>Car Description:</h2>
                <form>
                Brand: <input type="text" name="brand" onChange={(event) => {this.setState({brand: event.target.value})}} align="left" /> <br />
                Model: <input type="text" name="model" onChange={(event) => {this.setState({model: event.target.value})}} align="left" /> <br />
                Category: <select name="category" onChange={(event) => {this.setState({category: event.target.value})}} align="left">
                    {optionCategoryList}
                    </select> <br />
                Transmission: <select name="transmission" onChange={(event) => {this.setState({transmission: event.target.value})}} align="left">
                    {optionTransmissionList}
                    </select> <br />
                Number of Passengers: <input type="text" name="numberOfPassengers" onChange={(event) => {this.setState({numberOfPassengers: event.target.value})}} align='left' /> <br />
                Price per Hour: <input type="text" name="pricePerHour" onChange={(event) => {this.setState({pricePerHour: event.target.value})}} align='left' /> <br />
                <button onClick={this.handleSubmit}>Submit</button>
                </form>
            </div>
        )
    }
}

export default AddCar
