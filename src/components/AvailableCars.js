import React, { Component } from 'react'
import axios from 'axios';
import { Link } from 'react-router-dom';
import Navigation from './Navigation';

class AvailableCarsDescription extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             cars: []
        }
    }
    
    componentDidMount() {
        console.log(this.props);
        axios.get(`http://localhost:8080/rentalcars/available/${this.props.match.params.pickupDate}/${this.props.match.params.returnDate}`).then(res => {
            const cars = res.data;
            this.setState({cars});
        });
    }

    render() {
        console.log('AvailableCars');
        console.log(this.props);

        console.log('AvailableCars');
        console.log(this.props);
        

        let pickupDate = new Date(this.props.match.params.pickupDate).toLocaleString(['ro-RO']);
        let returnDate = new Date(this.props.match.params.returnDate).toLocaleString(['ro-RO']);

        let carsList = this.state.cars.map((car) => 
        <tr key={car.id}>
            <td>{car.brand}</td>
            <td>{car.model}</td>
            <td>{car.category}</td>
            <td>{car.transmission}</td>
            <td>{car.numberOfPassengers}</td>
            <td>{car.pricePerHour}</td>
            <td><Link to={`/reservation/${car.id}/${this.props.match.params.pickupDate}/${this.props.match.params.returnDate}`}>Reserve</Link></td>
        </tr>       
    )

        return (
            <div>
                <div align='left'>
                    <Navigation />
                </div>
                <h2>
                    Available cars between {pickupDate} and {returnDate}
                </h2>

                <table align='center' border='1px'>
                    <thead>
                        <tr>
                            <th>Brand</th>
                            <th>Model</th>
                            <th>Category</th>
                            <th>Transmission</th>
                            <th>Number of passegers</th>
                            <th>Price per hour</th>
                        </tr>
                    </thead>
                    <tbody>
                            {carsList}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default AvailableCarsDescription
