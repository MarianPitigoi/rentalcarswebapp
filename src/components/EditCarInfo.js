import React, { Component } from 'react';
import axios from 'axios';
import Navigation from './Navigation';

export class EditCarInfo extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            car:{},
            carInfo:[],
            newAvailableState: false,
            newOdometer: 0
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    componentDidMount() {
        // console.log(this.props);
        axios.get('http://localhost:8080/rentalcars/' + this.props.match.params.carId).then(res => {
            const car = res.data;
            const carsInfoList = [...car.carsInfo];
            const carsInfoFiltred = carsInfoList.filter(carInfo => carInfo.vin === this.props.match.params.carInfoVin);
            const carInfo = {...carsInfoFiltred[0]};
            this.setState({car, carInfo, newAvailableState: carInfo.available});
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        const data = {
            // carId: this.state.car.id,
            vin: this.state.carInfo.vin,
            available: this.state.newAvailableState,
            numberPlate: this.state.carInfo.numberPlate,
            odometer: this.state.newOdometer
        }
        axios.put(`http://localhost:8080/carsInfo`, data).then(() => {
            window.history.back();
        })
    }

    render() {
        console.log('State');
        console.log(this.state);
        let isAvailable = (available) => available ? 'available' : 'unavailable';

        return (
            <div>
                <div align='left'>
                    <Navigation />
                </div>
                <h2>Rental Cars</h2>
                Brand: {this.state.car.brand}<br />
                Model: {this.state.car.model}<br/>
                Category: {this.state.car.category}<br />
                Transmission: {this.state.car.transmission}<br />
                Number of Passengers: {this.state.car.numberOfPassengers}<br />
                Price per Hour: {this.state.car.pricePerHour}<br /> 
                <h2>Details</h2>
                <form>
                VIN: {this.state.carInfo.vin} <br />
                Available: <select name="available" onChange={(event) => this.setState({newAvailableState: event.target.value})} align='left'>
                        <option value={this.state.carInfo.available}>{isAvailable(this.state.carInfo.available)}</option>
                        <option value='true'>available</option>
                        <option value='false'>unavailable</option>
                        </select> <br />
                Number Plate:{this.state.carInfo.numberPlate} <br />
                Odometer: From {this.state.carInfo.odometer} To: <input type="text" name="odometer" 
                                        onChange={(event) => this.setState({newOdometer: event.target.value})} align='left' />  <br />
                <button onClick={this.handleSubmit}>Submit</button>
                </form>
            </div>
        )
    }
}

export default EditCarInfo
