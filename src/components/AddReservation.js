import React, { Component } from 'react';
import axios from 'axios';
import Navigation from './Navigation';

class AddReservation extends Component {
    constructor(props) {
        super(props)

        this.state = {
            car: {
                id: 0,
                brand: '',
                model: '',
                category: '',
                transmission: '',
                numberOfPassengers: 0,
                pricePerHour: 0,
            },
            user: {
                firstName: '',
                middleName: '',
                lastName: '',
                email: '',
                phoneNumber: ''
            },
            reservation:{
                pickupDate: new Date(Date.parse(this.props.match.params.pickupDate)),
                returnDate: new Date(Date.parse(this.props.match.params.returnDate))
            }
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {

        axios.get(`http://localhost:8080/rentalcars/${this.props.match.params.carId}`).then(res => {
            const car = res.data;
            this.setState({car});
        });
    }

    handleChange(event){
        let name = event.target.name;
        let value = event.target.value;
        let user = {...this.state.user};
        user[name] = value;
        this.setState({user});
    }

    handleSubmit(event){
        event.preventDefault();
        const data = {
            userFirstName: this.state.user.firstName,
            userMiddleName: this.state.user.middleName,
            userLastName: this.state.user.lastName,
            userEmail: this.state.user.email,
            userPhoneNumber: this.state.user.phoneNumber,
            carId: this.state.car.id,
            pickupDate: this.state.reservation.pickupDate,
            returnDate: this.state.reservation.returnDate
        }


        axios.post(`http://localhost:8080/rentalcars/users`, data).then((res) => {
            let reservations = {...res.data.reservations}; 
            const lastReservation = reservations[Object.keys(reservations).length - 1];
            this.props.history.push(`/reservation/${lastReservation.id}`);
        });
    }

    render() {
        console.log('AddReservation');
        console.log(this.props);

        let pickupDate = this.state.reservation.pickupDate.toLocaleString(['ro-RO']);
        let returnDate = this.state.reservation.returnDate.toLocaleString(['ro-RO']);

        return (
            <div align='left'>
                <div align='left'>
                    <Navigation />
                </div>
                <h2>Rental Car:</h2>
                Brand: {this.state.car.brand} <br />
                Model: {this.state.car.model} <br />
                Category: {this.state.car.category} <br />
                Transmission: {this.state.car.transmission} <br />
                Number of Passengers: {this.state.car.numberOfPassengers} <br />
                Price per Hour: {this.state.car.pricePerHour} <br />
                <h2>Rental Period:</h2>
                From: {pickupDate} <br />
                To: {returnDate}

                <h2>User Data</h2>
                <form>
                    First Name: <input type="text" name="firstName" onChange={this.handleChange} align='left'/> <br /> 
                    Middle Name: <input type="text" name="middleName" onChange={this.handleChange} align='left'/> <br /> 
                    Last Name: <input type="text" name="lastName" onChange={this.handleChange} align='left'/> <br /> 
                    Email: <input type="text" name="email" onChange={this.handleChange} align='left'/> <br /> 
                    Phone Number: <input type="text" name="phoneNumber" onChange={this.handleChange} align='left'/> <br />
                    <button onClick={this.handleSubmit}>Submit Reservation</button>
                </form>
            </div>
        )
    }
}

export default AddReservation
