import axios from 'axios';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Navigation from './Navigation';

class CarInfo extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            id: 0,
            brand:'',
            model:'',
            category:'',
            transmission:'',
            numberOfPassengers:0,
            pricePerHour:0,
            carsInfo: []
        }
        this.handleDelete = this.handleDelete.bind(this);
    }
    
    componentDidMount() {
        // console.log(this.props);
        axios.get('http://localhost:8080/rentalcars/' + this.props.match.params.carId).then(res => {
            this.setState(res.data);
        });
    }

    handleDelete(carInfo) {
        axios.delete('http://localhost:8080/carsInfo/' + carInfo.vin).then(() => {
            let updateCarsInfo = [...this.state.carsInfo].filter(item => item.vin !== carInfo.vin);
            this.setState({carsInfo: updateCarsInfo});
        }) 
    }

    render() {
        console.log('CarInfo');
        console.log(this.props);

        let editCarInfo = (carId, carInfoVin) => {
            if(carInfoVin != null){
                return <Link to={'/editcarinfo/' + carId + '/' + carInfoVin}>Edit Car Info</Link>
            }
            return ;
        }

        let isAvailable = (available) => available ? 'available' : 'unavailable'; 

        let carInfoList = this.state.carsInfo.map((carInfo,index) =>
            <tr key={index}>
                <td>{carInfo.vin}</td>
                <td>{isAvailable(carInfo.available)}</td>
                <td>{carInfo.numberPlate}</td>
                <td>{carInfo.odometer}</td>
                <td>{editCarInfo(this.state.id,carInfo.vin)}</td>
                <td><Link to={'/rentalcars/' + this.state.id} onClick={() => this.handleDelete(carInfo)}>Delete</Link></td>
            </tr>
        )

        // console.log(this.state.details);
        return (
            <div>
                <div align='left'>
                    <Navigation />
                </div>
                <h2>Rental Cars</h2>
                Brand: {this.state.brand}<br />
                Model: {this.state.model}<br/>
                Category: {this.state.category}<br />
                Transmission: {this.state.transmission}<br />
                Number of Passengers: {this.state.numberOfPassengers}<br />
                Price per Hour: {this.state.pricePerHour}<br /> 
                <h2>Details</h2>
                <table align='center' border='1px'>
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Available</th>
                            <th>Number Plate</th>
                            <th>Odometer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {carInfoList}
                    </tbody>
                </table>
                <Link to={this.state.id + '/addcarinfo'}>Add new</Link>
            </div>
        )
    }
}

export default CarInfo
