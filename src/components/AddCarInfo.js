import axios from 'axios';
import React, { Component } from 'react';
import Navigation from './Navigation';

class AddCarInfo extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            id: 0,
            brand: '',
            model: '',
            category: '',
            transmission: '',
            numberOfPassengers: 0,
            pricePerHour: 0,
            carInfo: {
                vin: '',
                availableForReservation: true,
                rented: false,
                numberPlate: '',
                odometer: 0
            }
        }
        
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        axios.get(`http://localhost:8080/rentalcars/${this.props.match.params.carId}`).then(res => {
            this.setState(res.data);
        });
    }

    handleChange(event){
        const value = event.target.value;
        const name = event.target.name;
        let carInfo = {...this.state.carInfo};
        carInfo[name] = value;
        this.setState({carInfo});
    }

    handleSubmit(event) {
        event.preventDefault();
        const data = {
            carId: this.state.id,
            vin: this.state.carInfo.vin,
            available: this.state.carInfo.availableForReservation,
            numberPlate: this.state.carInfo.numberPlate,
            odometer: this.state.carInfo.odometer
        }
        console.log("this.state.id = " + this.state.id + " carVin: " + this.state.carInfo.vin);
        axios.put(`http://localhost:8080/rentalcars`, data).then(() => {
            window.history.back();
        })
    }
    
    render() {
        console.log('AddCarInfo');
        console.log(this.props);

        return (
            <div align='left'>
                <div align='left'>
                    <Navigation />
                </div>
                <h2>Rental Car:</h2>
                Brand: {this.state.brand} <br />
                Model: {this.state.model} <br />
                Category: {this.state.category} <br />
                Transmission: {this.state.transmission} <br />
                Number of Passengers: {this.state.numberOfPassengers} <br />
                Price per Hour: {this.state.pricePerHour} <br />
                <h2> Car Info </h2>
                <form>
                    VIN: <input type="text" name="vin" onChange={this.handleChange} align='left'/> <br />                    
                    Available for Reservation: <select name="available" onChange={this.handleChange} align='left'>
                        <option value='true'>available</option>
                        <option value='false'>unavailable</option>
                        </select> <br />
                    Number Plate: <input type="text" name="numberPlate" onChange={this.handleChange} align='left'/> <br />
                    Odometer: <input type="text" name="odometer" onChange={this.handleChange} align='left' /> <br /> 
                    <button onClick={this.handleSubmit}>Submit</button>
                </form>
            </div>
        )
    }
}

export default AddCarInfo
