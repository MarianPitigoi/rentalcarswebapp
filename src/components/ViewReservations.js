import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Navigation from './Navigation';
// import CarInfo from './CarInfo';

class ViewReservations extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             reservations: []
        }
    }
    
    componentDidMount(){
        axios.get('http://localhost:8080/rentalcars/reservations').then(res =>{
            const reservations = res.data;
            this.setState({reservations});
            console.log(this.state);
        })
    }

    render() {
        let isAvailable = (available) => (available) ? 'available' : 'unavailable';
        let addCarInfo = (reservation) => {
            if(reservation.carInfoVin == null){
                return <Link to={'/assigncartoreservation/' + reservation.carId + '/' + reservation.reservationId + '/' + reservation.reservationPickupDate + '/' + reservation.reservationReturnDate} >Add Car</Link>
            }
            return ;
        }
        
        let reservations = this.state.reservations.map((reservation,index) => {
            let pickupDate = new Date(reservation.reservationPickupDate).toLocaleString(['ro-RO']);
            let returnDate = new Date(reservation.reservationReturnDate).toLocaleString(['ro-RO']);
            
            return  <tr key={index}>
                    <td>{pickupDate}</td>
                    <td>{returnDate}</td>
                    <td>{reservation.reservationTotalPrice} &euro;</td>
                    <td>{reservation.userFirstName}</td>
                    <td>{reservation.userMiddleName}</td>
                    <td>{reservation.userLastName}</td>
                    <td>{reservation.userEmail}</td>
                    <td>{reservation.userPhoneNumber}</td>
                    <td>{reservation.carBrand}</td>
                    <td>{reservation.carModel}</td>
                    <td>{reservation.carCategory}</td>
                    <td>{reservation.carTransmission}</td>
                    <td>{reservation.carNumberOfPassengers}</td>
                    <td>{reservation.carPricePerHour} &euro;</td>
                    <td>{reservation.carInfoVin}</td>
                    <td>{isAvailable(reservation.carInfoAvailable)}</td>
                    <td>{reservation.carInfoNumberPlate}</td>
                    <td>{reservation.carInfoOdometer}</td>
                    <td>{addCarInfo(reservation)}</td>
                    </tr>
        });

        return (
            <div>
                <div align='left'>
                    <Navigation />
                </div>
                <h2>Resevations</h2>
                <table border="1px">
                    <thead>
                        <tr>
                            <th>Pickup Date</th>
                            <th>Return Date</th>
                            <th>Total Price</th>
                            <th>User First Name</th>
                            <th>User Middle Name</th>
                            <th>User Last Name</th>
                            <th>User Email</th>
                            <th>User Phone Number</th>
                            <th>Car Brand</th>
                            <th>Car Model</th>
                            <th>Car Category</th>
                            <th>Car Transmission</th>
                            <th>Car Number of Passengers</th>
                            <th>Car Pricer per Hour</th>
                            <th>Car Vin</th>
                            <th>Car Availability</th>
                            <th>Car Number Plate</th>
                            <th>Car Odometer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {reservations}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ViewReservations
